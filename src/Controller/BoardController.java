package Controller;

import Connection.SecureChatClient;
import Model.Board;
import Model.Dice;
import Model.Player;
import Model.Portion;
import View.Console;

import java.util.*;

public class BoardController {

    static BoardController instance;

    private Board board;

    private int diceCount = 5;
    private int minPlayers = 2;
    private int maxPlayers = 5;
    private List<Integer> sushisValues = new ArrayList<>(Arrays.asList(1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6));
    private List<Integer> aretesValues = new ArrayList<>(Arrays.asList(-1, -1, -1, -1, -1, -2, -2, -2, -2, -3, -3, -4));
    private List<String> diceValues = new ArrayList<>(Arrays.asList("sushi", "sushi", "aretes", "aretes", "baguettes bleu", "baguettes rouge"));

    private ServerThreadController serverThreadController;
    private ClientThreadController clientThreadController;

    public static BoardController getInstance() throws Exception {
        if (instance == null) {
            instance = new BoardController();
        }
        return instance;
    }

    public BoardController () throws Exception {
        this.serverThreadController = new ServerThreadController();
        this.serverThreadController.start();

        this.clientThreadController = new ClientThreadController();
        this.clientThreadController.start();

        createBoard();

        game();
    }

    public void createBoard() {
        this.board = new Board();
        createDices();
        createPortion();
        createPlayers();
        Console.showPortions(this.board.getSushis(), this.board.getAretes());
    }

    public void createPlayers() {
        int choice = 0;

        while(!(this.minPlayers <= choice && choice <= this.maxPlayers)) {
            choice = Console.inputInt(String.format("Nombre de " +
                    "joueurs (entre %d et %d joueurs)\n> ", this.minPlayers, this.maxPlayers));
        }

        String name;
        for (int i = 0; i < choice; i++) {
            name = Console.inputStr("Nom du joueur " + (i+1) + "\n> ");
            this.board.addPlayer(new Player(i, name));
        }

    }

    public void createDices() {
        List<Dice> dices = new ArrayList<>();
        for (int i = 0; i < this.diceCount; i++) {
            dices.add(new Dice(this.diceValues));
        }
        this.board.setDices(dices);
    }

    public void createPortion() {
        List<Portion> sushis = new ArrayList<>();
        List<Portion> aretes = new ArrayList<>();
        for (Integer val : this.sushisValues) {
            sushis.add(new Portion("sushi", val));
        }
        Collections.shuffle(sushis);
        this.board.setSushis(sushis);

        for (Integer val : this.aretesValues) {
            aretes.add(new Portion("aretes", val));
        }
        Collections.shuffle(aretes);
        this.board.setAretes(aretes);
    }

    public void game() {
        while (this.board.countAretes() > 0 && this.board.countSushis() > 0) {
            for (Player player : this.board.getPlayers()) {
                Console.print("\n" + player.getName() + " joue");
                Console.print("\n" + player.toString());
                this.board.resetDices();

                this.playerTurn(player);

            }
        }
        List<Player> scoreboard = this.board.getScoreboard();
        Console.showScoreboard(scoreboard);
    }

    private void playerTurn(Player player) {
        int actions = 4;

        while (actions > 0) {

            int choice = this.chooseAction();

            switch (choice) {
                case 1:
                    actions = this.actionRollDice(actions);
                    break;
                case 2:
                    actions = this.actionPickSushi(player, actions);
                    break;
                case 3:
                    actions = this.actionPickAretes(player, actions);
                    break;
                case 4:
                    actions = this.actionStealSushi(player, actions);
                    break;
                case 5:
                    actions = this.actionStealAretes(player, actions);
                    break;
            }

        }


    }

    private int chooseAction() {
        int choice = 0;
        Console.showPortions(this.board.getSushis(), this.board.getAretes());
        while (!(0 < choice && choice < 6)) {
            choice = Console.inputInt("\n" +
                    "\n1 - Lancer les dés" +
                    "\n2 - Prendre un sushi (" + this.board.countDiceByRolledValue("sushi") + " dé(s) sushi)" +
                    "\n3 - Prendre des aretes (" + this.board.countDiceByRolledValue("aretes") + " dé(s) aretes)" +
                    "\n4 - Prendre un sushi chez un joueur (" + this.board.countDiceByRolledValue("baguettes bleu") + " baguettes bleu)" +
                    "\n5 - Prendre des aretes chez un joueur (" + this.board.countDiceByRolledValue("baguettes rouge") + " baguettes rouge)" +
                    "\n> ");
        }

        return choice;
    }

    private int actionRollDice(int actions) {
        if (this.board.countUnsavedDices() == 0 || actions == 1) {
            Console.print("\nVous ne pouvez plus relancer");
        }
        else {
            this.board.rollUnsavedDices();
            Console.showRolledDices(this.board.getDices());
            this.saveDices();
            actions -= 1;
        }

        return actions;
    }

    public void saveDices() {
        int savedDiceCount = 0;
        boolean loop = true;

        while (loop) {
            int choice = -1;
            List<Integer> unsavedDiceIndexes = this.board.getUnsavedDiceIndexes();
            unsavedDiceIndexes.add(0);

            while (!(unsavedDiceIndexes.contains(choice))) {
                choice = Console.inputInt("\nChoisissez un dé non enregistré (soit 0 pour finir)" +
                        "\n"+unsavedDiceIndexes+"> ");
            }

            if (choice == 0 && savedDiceCount != 0) {
                loop = false;
            }
            else if (choice == 0) {
                Console.print("\nVous devez garder au moins un dé");
            }
            else {
                this.board.getDices().get(choice-1).setSaved(true);
                savedDiceCount += 1;
            }
        }
    }

    private int actionPickSushi(Player player, int actions) {
        if (this.board.countSavedDices() == 0) {
            Console.print("\nVous devez lancer les dés");
        }
        else {
            Portion portion = this.board.getAvailablePortion("sushi");
            if (portion != null) {
                this.board.popSushi(portion);
                player.addToSushiList(portion);
                actions = 0;
            }
            else {
                Console.print("\nVous ne pouvez pas");
            }
        }

        return actions;
    }

    private int actionPickAretes(Player player, int actions) {
        if (this.board.countSavedDices() == 0) {
            Console.print("\nVous devez lancer les dés avant");
        }
        else {
            Portion portion = this.board.getAvailablePortion("aretes");
            if (portion != null) {
                this.board.popAretes(portion);
                player.addToAretesList(portion);
                actions = 0;
            }
            else {
                Console.print("\nVous ne pouvez pas");
            }
        }

        return actions;
    }

    private int actionStealSushi(Player player, int actions) {
        if (this.board.countSavedDices() == 0) {
            Console.print("\nVous devez lancer les dés avant");
        }
        else if (!this.board.playersHasPortion("sushi")) {
            if (actions == 1) {
                actions = applyPenalty(player);
            }
            else {
                Console.print("\nLes autres joueurs n'ont pas assez de sushis");
            }
        }
        else {
            int count = this.board.countDiceByRolledValue("baguettes bleu");

            if (count < 3) {
                Console.print("\nVous n'avez pas assez de baguettes bleu");
            }
            else if (count == 3) {
                Console.showPlayers(this.board.getOtherPlayers(player.getId()));
                List<Integer> ids = this.board.getOtherPlayersIds(player.getId());

                int choice = -1;

                while (!ids.contains(choice)) {
                    choice = Console.inputInt("\nA qui voulez vous voler le sushi ?" +
                            "\n" + ids.toString() + "> ");

                    Player targetPlayer = this.board.getPlayerById(choice);

                    if (targetPlayer != null) {
                        if (targetPlayer.countSushi() == 0) {
                            Console.print("\nLe joueur cible n'a pas assez de sushi");
                            choice = -1;
                        }
                        else {
                            Portion stolenSushi = targetPlayer.popLastSushi();
                            player.addToSushiList(stolenSushi);
                            Console.print("\nVous avez voler un sushi (" + stolenSushi.getValue() + ") à " + targetPlayer.getName());
                            actions = 0;
                        }
                    }
                }
            }
            else {
                Console.showPlayers(this.board.getOtherPlayers(player.getId()));
                List<Integer> ids = this.board.getOtherPlayersIds(player.getId());

                int choice = -1;

                while (!ids.contains(choice)) {
                    choice = Console.inputInt("\nA qui voulez vous voler le sushi ?" +
                            "\n" + ids.toString() + "> ");

                    Player targetPlayer = this.board.getPlayerById(choice);

                    if (targetPlayer != null) {
                        if (targetPlayer.countSushi() == 0) {
                            Console.print("\nLe joueur cible n'a pas assez de sushi");
                            choice = -1;
                        }
                        else {
                            int indexChoice = 0;
                            int targetPlayerSushiCount = targetPlayer.countSushi();
                            while (!(0 < indexChoice && indexChoice <= targetPlayerSushiCount)) {
                                indexChoice = Console.inputInt(targetPlayer.getName() + " a " + targetPlayerSushiCount +
                                        " sushis, le quel voulez vous lui volez ?");
                            }

                            Portion stolenSushi = targetPlayer.popSushiAt(indexChoice - 1);
                            player.addToSushiList(stolenSushi);
                            Console.print("\nVous avez voler un sushi (" + stolenSushi.getValue() + ") à " + targetPlayer.getName());
                            actions = 0;
                        }
                    }
                }

            }

        }
        return actions;
    }

    private int actionStealAretes(Player player, int actions) {
        if (this.board.countSavedDices() == 0) {
            Console.print("\nVous devez lancer les dés avant");
        }
        else if (!this.board.playersHasPortion("aretes")) {
            if (actions == 1) {
                actions = applyPenalty(player);
            }
            else {
                Console.print("\nLes autres joueurs n'ont pas assez d'arretes");
            }
        }
        else {
            int count = this.board.countDiceByRolledValue("baguettes rouge");

            if (count < 3) {
                Console.print("\nVous n'avez pas assez de baguettes rouge");
            }
            else if (count == 3) {
                Console.showPlayers(this.board.getOtherPlayers(player.getId()));
                List<Integer> ids = this.board.getOtherPlayersIds(player.getId());

                int choice = -1;

                while (!ids.contains(choice)) {
                    choice = Console.inputInt("\nA qui voulez vous voler les aretes ?" +
                            "\n" + ids + "> ");

                    Player targetPlayer = this.board.getPlayerById(choice);

                    if (targetPlayer != null) {
                        if (targetPlayer.countAretes() == 0) {
                            Console.print("\nLe joueur cible n'a pas assez d'aretes");
                            choice = -1;
                        }
                        else {
                            Portion stolenAretes = targetPlayer.popLastAretes();
                            player.addToAretesList(stolenAretes);
                            Console.print("\nVous avez voler des aretes (" + stolenAretes.getValue() + ") à " + targetPlayer.getName());
                            actions = 0;
                        }
                    }
                }
            }
            else {
                Console.showPlayers(this.board.getOtherPlayers(player.getId()));
                List<Integer> ids = this.board.getOtherPlayersIds(player.getId());

                int choice = -1;

                while (!ids.contains(choice)) {
                    choice = Console.inputInt("\nA qui voulez vous voler les aretes ?" +
                            "\n" + ids + "> ");

                    Player targetPlayer = this.board.getPlayerById(choice);

                    if (targetPlayer != null) {
                        if (targetPlayer.countAretes() == 0) {
                            Console.print("\nLe joueur cible n'a pas assez d'aretes");
                            choice = -1;
                        }
                        else {
                            int indexChoice = 0;
                            int targetPlayerAretesCount = targetPlayer.countAretes();
                            while (!(0 < indexChoice && indexChoice <= targetPlayerAretesCount)) {
                                indexChoice = Console.inputInt(targetPlayer.getName() + " a " + targetPlayerAretesCount +
                                        " aretes, le quel voulez vous lui volez ?");
                            }

                            Portion stolenAretes = targetPlayer.popAretesAt(indexChoice - 1);
                            player.addToAretesList(stolenAretes);
                            Console.print("\nVous avez voler des aretes (" + stolenAretes.getValue() + ") à " + targetPlayer.getName());
                            actions = 0;
                        }
                    }
                }

            }

        }
        return actions;
    }

    private int applyPenalty(Player player) {
        int actions;
        Portion portion = this.board.getPenaltyPortion();
        if (portion.getSymbol().equals("sushi")) {
            player.addToSushiList(portion);
        } else if (portion.getSymbol().equals("aretes"))
        {
            player.addToAretesList(portion);
        }

        Console.print("\nVous avez recu un " + portion.getSymbol() + " avec une valeur de " + portion.getValue());
        actions = 0;
        return actions;
    }
}
