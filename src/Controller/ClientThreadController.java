package Controller;

import Connection.SecureChatClient;

public class ClientThreadController extends Thread{
    public void run(){
        String[] args = new String[]{};
        try {
            SecureChatClient.main(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
