package Controller;

import Connection.SecureChatServer;

public class ServerThreadController extends Thread {

    public void run(){
        String[] args = new String[]{};
        try {
            SecureChatServer.main(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}