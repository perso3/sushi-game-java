package Model;

import View.Console;

import javax.sound.sampled.Port;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Board {

    private List<Player> players = new ArrayList<>();
    private List<Dice> dices = new ArrayList<>();
    private List<Portion> sushis = new ArrayList<>();
    private List<Portion> aretes = new ArrayList<>();

    public List<Player> getPlayers() {
        return this.players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public void addPlayer(Player player) {
        this.players.add(player);
    }

    public List<Dice> getDices() {
        return this.dices;
    }

    public void setDices(List<Dice> dices) {
        this.dices = dices;
    }

    public List<Portion> getSushis() {
        return this.sushis;
    }

    public void setSushis(List<Portion> sushis) {
        this.sushis = sushis;
    }

    public Portion popSushi(Portion sushi) {
        int index = this.sushis.indexOf(sushi);
        return this.popSushiAt(index);
    }

    public Portion popSushiAt(int index) {
        return this.sushis.remove(index);
    }

    public int countSushis() {
        return this.sushis.size();
    }

    public List<Portion> getAretes() {
        return this.aretes;
    }

    public void setAretes(List<Portion> aretes) {
        this.aretes = aretes;
    }

    public int countAretes() {
        return this.aretes.size();
    }

    public Portion popAretes(Portion aretes) {
        int index = this.aretes.indexOf(aretes);
        return this.popAretesAt(index);
    }

    public Portion popAretesAt(int index) {
        return this.aretes.remove(index);
    }

///////// PLAYER
    public List<Player> getOtherPlayers(int playerId) {
        List<Player> result = new ArrayList<>();

        for (Player player: this.players) {
            if (player.getId() != playerId) {
                result.add(player);
            }
        }

        return result;
    }

    public List<Integer> getOtherPlayersIds(int playerId) {
        List<Integer> result = new ArrayList<>();

        for (Player player: this.players) {
            if (player.getId() != playerId) {
                result.add(player.getId());
            }
        }

        return result;
    }

    public Player getPlayerById(int playerId) {
        Player result = null;

        for (Player player: this.players) {
            if (player.getId() == playerId) {
                result = player;
            }
        }

        return result;
    }

    public List<Player> getScoreboard() {
        List<Player> scoreboard = new ArrayList<>(this.getPlayers());

        scoreboard.sort(Comparator.comparing(Player::calculatePoints).reversed());

        return scoreboard;
    }

///////// PORTION
    public boolean playersHasPortion(String symbol) {
        int count = 0;
        List<Portion> portions = new ArrayList<>();
        for (Player player: players) {
            if (symbol.equals("sushi")) {
                portions = player.getSushiList();
            }
            else if (symbol.equals("aretes")) {
                portions = player.getAretesList();
            }
            if (portions.size() > 0) {
                count += 1;
            }
        }

        return players.size() == count;
    }

    public List<Portion> getPortionsBySymbol(String symbol) {
        List<Portion> result = null;
        if (symbol.equals("sushi")) {
            result = this.sushis;
        }
        else if (symbol.equals("aretes")) {
            result = this.aretes;
        }
        return result;
    }

    public Portion getMinPortionBySymbol(String symbol) {
        Portion result = null;
        List<Portion> portions = getPortionsBySymbol(symbol);
        for (Portion portion:portions) {
            if(result == null)
            {
                result = portion;
            }
            else if (result.getValue() > portion.getValue()){
                result = portion;
            }
        }
        return result;
    }

    public Portion getMaxPortionBySymbol(String symbol) {
        Portion result = null;
        List<Portion> portions = getPortionsBySymbol(symbol);
        for (Portion portion:portions) {
            if(result == null)
            {
                result = portion;
            }
            else if (result.getValue() < portion.getValue()){
                result = portion;
            }
        }
        return result;
    }

    public Portion getAvailablePortion(String symbol) {
        Portion result = null;
        int count = countDiceByRolledValue(symbol);

        List<Portion> portions = getPortionsBySymbol(symbol);

        if (countSavedDices() > 0) {
            if (0 < count && count < portions.size() + 1) {
                result = portions.get(count - 1);
            }
            else if (count == 0) {
                result = null;
            }
            else {
                result = getPenaltyPortion();
            }
        }
        return result;
    }

    public Portion getPenaltyPortion() {
        Portion portion = getMinPortionBySymbol("aretes");

        if (portion == null) {
            portion = getMinPortionBySymbol("sushi");
        }

        return portion;
    }

///////// DICE
    public void resetDices() {
        for (Dice dice: this.dices) {
            dice.reset();
        }
    }

    public void rollUnsavedDices() {
        for (Dice dice: this.dices) {
            if (!dice.isSaved()) {
                dice.roll();
            }
        }
    }

    public int countSavedDices() {
        int count = 0;
        for (Dice dice: this.dices) {
            if (dice.isSaved()) {
                count += 1;
            }
        }
        return count;
    }

    public int countUnsavedDices() {
        return dices.size() - countSavedDices();
    }

    public List<Integer> getUnsavedDiceIndexes() {
        List<Integer> indexes = new ArrayList<>();
        for (int i = 0; i < this.dices.size(); i++) {
            if (!this.dices.get(i).isSaved())
            {
                indexes.add(i+1);
            }
        }
        return indexes;
    }

    public int countDiceByRolledValue(String value) {
        int count = 0;
        for (Dice dice: this.dices) {
            if (dice.getRolledValue() != null) {
                if (dice.getRolledValue().equals(value)) {
                    count += 1;
                }
            }
        }
        return count;
    }
}
