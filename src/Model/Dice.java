package Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.IntToDoubleFunction;

public class Dice {

    private boolean saved = false;
    private List<String> values = new ArrayList<String>();
    private String rolledValue = null;

    public Dice(List<String> values) {
        this.values = values;
    }

    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public String getRolledValue() {
        return rolledValue;
    }

    public void setRolledValue(String rolledValue) {
        this.rolledValue = rolledValue;
    }

    public void reset() {
        this.rolledValue = null;
        this.saved = false;
    }

    public String roll() {
        Random random = new Random();
        this.rolledValue = this.values.get(random.nextInt(this.values.size()));
        return this.rolledValue;
    }

    @Override
    public String toString() {
        return (saved?"Saved ":"") + rolledValue;
    }
}
