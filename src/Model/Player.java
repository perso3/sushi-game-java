package Model;

import javax.sound.sampled.Port;
import java.util.ArrayList;
import java.util.List;

public class Player {
    private int id;
    private String name;
    private List<Portion> sushiList;
    private List<Portion> aretesList;

    public Player(int id, String name) {
        this.id = id;
        this.name = name;
        this.sushiList = new ArrayList<>();
        this.aretesList = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Portion> getAretesList() {
        return aretesList;
    }

    public void addToAretesList(Portion aretes) {
        this.aretesList.add(aretes);
    }

    public List<Portion> getSushiList() {
        return sushiList;
    }

    public void addToSushiList(Portion sushi) {
        this.sushiList.add(sushi);
    }

    public int countSushi() {
        return this.sushiList.size();
    }

    public int countAretes() {
        return this.aretesList.size();
    }

    public Portion getLastSushi() {
        Portion result = null;

        if (this.countSushi() > 0) {
            result = this.sushiList.get(this.countSushi()-1);
        }

        return result;
    }

    public Portion getLastAretes() {
        Portion result = null;

        if (this.countAretes() > 0) {
            result = this.aretesList.get(this.countAretes()-1);
        }

        return result;
    }

    public Portion popSushiAt(int index) {
        Portion result = null;
        if(this.countSushi() > 0 && index >= 0 && index < this.countSushi()) {
            result = this.sushiList.remove(index);
        }

        return result;
    }

    public Portion popAretesAt(int index) {
        Portion result = null;
        if(this.countAretes() > 0 && index >= 0 && index < this.countAretes()) {
            result = this.aretesList.remove(index);
        }

        return result;
    }

    public Portion popLastSushi() {
        return this.popSushiAt(this.countSushi()-1);
    }

    public Portion popLastAretes() {
        return this.popAretesAt(this.countAretes()-1);
    }

    public int calculatePoints() {
        int total = 0;

        int aretesCount = this.countAretes();
        List<Portion> newSushiList = this.sushiList.subList(0, aretesCount);

        total += countPointsBySymbol(newSushiList);
        total += countPointsBySymbol(this.aretesList);

        return total;
    }

    public static int countPointsBySymbol(List<Portion> portions) {
        int total = 0;
        for (Portion portion: portions) {
            total += portion.getValue();
        }
        return total;
    }

    public boolean greaterThanByPoints(Player other) {
        return this.calculatePoints() > other.calculatePoints();
    }

    @Override
    public String toString() {
        return  "\nNom ->" + name +
                "\nsushis (" + countSushi() + ")->" + getLastSushi() +
                "\naretes (" + countAretes() + ")->" + getLastAretes();
    }


}
