package View;

import Model.Dice;
import Model.Player;
import Model.Portion;

import java.util.List;
import java.util.Scanner;

public class Console {


    public static int inputInt(String message) {
        print(message);
        return new Scanner(System.in).nextInt();
    }

    public static String inputStr(String message) {
        print(message);
        return new Scanner(System.in).nextLine();
    }

    public static void print(String message) {
        System.out.print(message);
    }

    public static void showPortions(List<Portion> sushis, List<Portion> aretes) {
        print("\n\nSushis -> " + sushis.toString());
        print("\nAretes -> " + aretes.toString());
    }

    public static void showPlayers(List<Player> players) {
        for (Player player: players) {
            print(player.toString());
        }
    }

    public static void showRolledDices(List<Dice> dices) {
        int count = 1;
        for (Dice dice : dices) {
            print("\n" + count + " - " + dice.toString());
            count += 1;
        }
    }

    public static void showScoreboard(List<Player> scoreboard) {
        int count = 1;
        for (Player player:scoreboard) {
            print("\n" + count + " - " + player.getName());
        }
    }
}
